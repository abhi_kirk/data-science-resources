# Version Control for Data and ML Models

Data files and ML models should not be stored in the Gitlab repository, and should be tracked directly with Git. We will use a python library *dvc* to enable tracking while storing these files in *Google Drive*. 

Following are the basic steps to work with *dvc* with an example data file *~/repo/data/file.xml*:
- If you don't already have it, create a google account with a personal email address (you can use non-gmail email addresses here). 
- You should already have the invite to a shared folder with the same name as your project, e.g. *infinite-monkeys* or *qualia*. 
- Install *dvc* inside your project venv:
```
conda activate <venv>
pip install dvc
pip install dvc[gdrive]
``` 
- Initialize *dvc*:
```
dvc init -f
```
- Tracking *~/data/file.xml* with *dvc* and *git*:
```
ls -lh data
dvc add data/file.xml
git add data/.gitignore data/file.xml.dvc
git commit -m "add raw data"
```
- Get code from Google Drive:
![None](gdrive_code.png "Google Drive Code Highlighted")
```
dvc remote add -d storage gdrive://<dir code from Google Drive>
git commit .dvc/config -m "Configure remote storage"
dvc push
```
- Follow instructions to enter a one-time verification code to authenticate
- You will not be able to see the pushed *file.xml* on the Google Drive since it is encrypted (but it exists!)

To verify that the file exists, delete it from your local dir and pull from Google Drive:
```
rm -f data/file.xml
rm -rf .dvc/cache
dvc pull
```

- *DVC* website for more information: https://dvc.org/doc 
- Even more info! https://dvc.org/blog/scipy-2020-dvc-poster 
- Watch this video for details: https://www.youtube.com/watch?v=kLKBcPonMYw&t=288s 

**More details on *dvc* coming soon!**