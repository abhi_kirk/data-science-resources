# Git/Gitlab Workflow

- Pull exisiting repository to local machine:
```
git pull <url>
```

- Pycharm will auto-recognize git if .git (hidden) folder is in the pycharm project directory. 

- Create your own development branch (example: *dev_abhi*):
```
git branch <branch name>
git checkout <branch name>
```
- Create a .gitignore file and add any files/dirs you don't want to push to Gitlab (such as data or model files, test python scripts, etc.):
```
touch .gitignore
```
- Work on this branch in pycharm (i.e. make commits and pushes)
    - Turn on auto-add in pycharm with: *Preferences | Version control | Confirmation | When files are created | Add silently*
    - Commit with pycharm using with *View | Tool Windows | Git*, or commit at Terminal/Git bash:
    ```
    git commit <filename(s)> "message"
    git status
    ```
    - Commits should be to your own git branch, and not the main branch
    Best practice is to commit whenever a significant block of code is finished and locally tested (at least once per coding session)
    - Push from pycharm, or from the Terminal/Git bash
        - Each push does not have to follow each commit 
        - Push is to sync your local git with gitlab repo
        - Best practice is to do several commits and then a single push for the day
    ```
    git push
    git status
    ```
- Create a merge request (to main repo) on Gitlab
    - Merge request can be for multiple pushes
    - Always assign another developer as a reviewer, along with any comments you want the reviewer to see

