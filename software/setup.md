# Data Science Project Setup

### Basics
- Choose one personal email address to use everywhere on this project
- Watch the Agile intro video: https://youtu.be/Z9QbYZh1YXY  

### Anaconda
- For: Package management with conda virtual environments
- Install individual edition:
    * Download link: https://www.anaconda.com/products/individual

### Git
- For: Code version control
- Install:
    * Instructions: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git 
    * MacOS: 
        * Can install HomeBrew, and then install Git: https://git-scm.com/download/mac (HomeBrew might fail on the first run, simply run it again and it should succeed)
        * Git bash will be accessed directly via the MacOS terminal; type git in Terminal to confirm
    * Windows: After installation, you should see ‘git bash’ options when doing a right-click inside any directory

### Gitlab
- For: Project collaboration and Agile CI/CD
- Create a public account: https://gitlab.com/ 
    * We will use Gitlab instead of GitHub because CI/CD is very well integrated with Gitlab
    * Add your SSH key to Gitlab:
        * MacOS:
            * Open Terminal
            * Type the following with your email address: 
            ```
            ssh-keygen -t ed25519 -C "your_email@example.com"
            ```
            * When prompted for save location, press return
            * Copy the key with the command: 
            ```
            pbcopy < ~/.ssh/id_ed25519.pub
            ```
            * Paste the key in Gitlab >> User Settings >> SSH Keys
        * Windows:
            * From the Start menu, open Git GUI >> Help >> Show SSH Key
            * Generate SSH key and Copy to Clipboard
            * Paste the key in Gitlab>>User Settings>>SSH Keys

### Jira
- For: Software Project Management
- Create a public account: [Link](https://www.atlassian.com/software/jira?&aceid=&adposition=&adgroup=89541891022&campaign=9124878120&creative=542569520296&device=c&keyword=jira%20download&matchtype=e&network=g&placement=&ds_kids=p51241495794&ds_e=GOOGLE&ds_eid=700000001558501&ds_e1=GOOGLE&gclsrc=ds)
- Watch the intro video: https://youtu.be/PQa3NFB_LRg 

### PyCharm
- For: Python Integrated Development Environment (IDE)
- Install community edition:
    * Download link: https://www.jetbrains.com/pycharm/download/#section=mac 

### Slack
- For: Team communication
- Install:
    * MacOS: https://slack.com/downloads/mac 
    * Windows: https://slack.com/downloads/windows 

### Visual Studio Code
- For: Javascript and Markdown IDE
- Install: https://code.visualstudio.com/download 
