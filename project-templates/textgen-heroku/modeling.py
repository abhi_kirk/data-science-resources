import os, warnings
from numpy import array
from pickle import dump, load
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Dense, LSTM, Embedding
from tensorflow.keras.preprocessing.sequence import pad_sequences
import tensorflow
tensorflow.get_logger().setLevel("ERROR")
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
warnings.filterwarnings("ignore", category=UserWarning)


class Modeling:
    def __init__(self, c):
        self.c = c
        self.tokenizer = Tokenizer()

    def training_data(self, sequences_text):
        self.tokenizer.fit_on_texts(sequences_text)
        sequences_int = self.tokenizer.texts_to_sequences(sequences_text)
        vocab_size = len(self.tokenizer.word_index) + 1
        sequences_int = array(sequences_int)
        X, y = sequences_int[:, :-1], sequences_int[:, -1]
        y = to_categorical(y, num_classes=vocab_size)
        seq_length = X.shape[1]
        return X, y, seq_length, vocab_size

    def model_struct(self, vocab_size, seq_len):
        model = Sequential()
        model.add(Embedding(input_dim=vocab_size,
                            output_dim=self.c.config['model']['embedding']['dims'],
                            input_length=seq_len))
        model.add(LSTM(self.c.config['model']['lstm']['mem_cells'],
                       return_sequences=True))
        model.add(LSTM(self.c.config['model']['lstm']['mem_cells']))
        model.add(Dense(self.c.config['model']['lstm']['mem_cells'],
                        activation='relu'))
        model.add(Dense(vocab_size, activation='softmax'))
        return model

    def model_fit(self, model, X, y):
        model.compile(loss='categorical_crossentropy',
                      optimizer='adam',
                      metrics=['accuracy'])
        model.fit(X, y,
                  batch_size=self.c.config['model']['batch_size'],
                  epochs=self.c.config['model']['epochs'])
        return model

    def train_model(self, text_seqs):
        X, y, seq_len, vocab_size = self.training_data(text_seqs)
        model_struct = self.model_struct(vocab_size, seq_len)
        model_fit = self.model_fit(model_struct, X, y)
        return model_fit, self.tokenizer

    def save_models(self, model_dict):
        save_path = os.path.join(self.c.base_dir,
                                 self.c.models_dir)
        for name, model in model_dict.items():
            if 'langmodel' in name:
                model.save(os.path.join(save_path,
                                        self.c.config['model']['langmodel_file']))
            else:
                dump(model, open(os.path.join(save_path,
                                              name),
                                 'wb'))

    def load_models(self):
        load_path = os.path.join(self.c.base_dir,
                                 self.c.models_dir)
        tokenizer = load(open(os.path.join(load_path,
                                           self.c.config['model']['tok_file']),
                              'rb'))
        langmodel = load_model(os.path.join(load_path,
                                            self.c.config['model']['langmodel_file']))
        return langmodel, tokenizer

    def generate_seq(self, model_, tokenizer_, seed_text_):
        result = list()
        in_text = seed_text_
        for _ in range(self.c.config['inference']['gen_words']):
            encoded = tokenizer_.texts_to_sequences([in_text])[0]
            encoded = pad_sequences([encoded],
                                    maxlen=self.c.config['training']['len_input'],
                                    truncating='pre')
            yhat = model_.predict_classes(encoded, verbose=0)
            out_word = ''
            for word, index in tokenizer_.word_index.items():
                if index == yhat:
                    out_word = word
                    break
            in_text += ' ' + out_word
            result.append(out_word)
        return ' '.join(result)

    def inference(self, in_txt):
        langmodel, tokenizer = self.load_models()
        gen_seq = self.generate_seq(model_=langmodel,
                                    tokenizer_=tokenizer,
                                    seed_text_=in_txt)
        return gen_seq
