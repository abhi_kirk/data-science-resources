import os
import string


class ETL:
    def __init__(self, c):
        self.c = c

    def convert_to_seqs(self, file_path):
        if os.path.isfile(file_path):
            tokens = self.preprocess(file_path)
            sequences = self.get_seqs(tokens)
            return sequences
        else:
            raise FileNotFoundError(f"{self.c.mode}: Input data file not found.")

    @staticmethod
    def load_txt(filename):
        file = open(filename, 'r')
        text = file.read()
        file.close()
        return text

    def save_txt_list(self, text_list, filename):
        # data = '\n'.join(text_list)
        data = text_list
        file = open(os.path.join(self.c.base_dir,
                                 self.c.models_dir,
                                 filename),
                    'w')
        file.write(data)
        file.close()

    def get_seqs(self, tokens):
        length = self.c.config['training']['len_input'] + \
                 self.c.config['training']['len_output']
        sequences = list()
        for i in range(length, len(tokens)):
            seq = tokens[i - length:i]
            line = ' '.join(seq)
            sequences.append(line)
        return sequences

    def preprocess(self, filename):
        text = self.load_txt(filename=filename).replace('--', ' ')
        tokens = text.split()
        if self.c.config['preprocess']['rem_punct']:
            table = str.maketrans('', '', string.punctuation)
            tokens = [w.translate(table) for w in tokens]
        if self.c.config['preprocess']['rem_nonalpha']:
            tokens = [word for word in tokens if word.isalpha()]
        if self.c.config['preprocess']['lowercase']:
            tokens = [word.lower() for word in tokens]
            return tokens

