import os
from init import Init
from etl import ETL
from modeling import Modeling
from viz import app, server


def training(c, etl, modeling):
    file_path = os.path.join(c.base_dir,
                             c.data_dir,
                             c.config['training']['file'])
    sequences = etl.convert_to_seqs(file_path=file_path)
    language_model, tokenizer = modeling.train_model(text_seqs=sequences)
    modeling.save_models({c.config['model']['langmodel_file']: language_model,
                          c.config['model']['tok_file']: tokenizer})


def main():
    base_dir = os.getcwd()
    data_dir, models_dir = "data", "models"
    init = Init(base_dir=base_dir,
                data_dir=data_dir,
                models_dir=models_dir,
                config="config.yml")
    c_ = init.c()
    etl, modeling = ETL(c=c_), Modeling(c=c_)

    if c_.config['mode'] == 'training':
        training(c_, etl, modeling)
    elif c_.config['mode'] == 'inference':
        app.run_server(debug=True)
    else:
        raise OSError("Incorrect mode in config.")


if __name__ == '__main__':
    main()
