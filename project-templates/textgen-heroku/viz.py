import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from modeling import Modeling
from init import Init
import os

conv_hist = []
base_dir, data_dir, models_dir = os.getcwd(), "data", "models"
init = Init(base_dir=base_dir,
            data_dir=data_dir,
            models_dir=models_dir,
            config="config.yml")
c_ = init.c()

external_stylesheets = [
    {
        "href": "https://fonts.googleapis.com/css2?"
                "family=Lato:wght@400;700&display=swap",
        "rel": "stylesheet",
    },
]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.title = "Qualia: Abhi Jain"
server = app.server
app.css.append_css({"external_url": "https://codepen.io/chriddyp/pen/bWLwgP.css"})

app.layout = html.Div([
    html.Div(
        children=[
            html.P(children="📚", className="header-emoji"),
            html.H1(
                children="Simple Text Generator", className="header-title"
            ),
            html.P(
                children=["Type any text and the AI "
                          "will complete it for you. "
                          "Hogwarts DevOps project group ", html.Br(),
                          "-- Abhi Jain"],
                className="header-description",
            ),
        ],
        className="header",
    ),
    html.H3('Complete Your Sentences!',
            style={'text-align': 'center'}),
    html.Div([
        html.Div([
            html.Table([
                html.Tr([
                    html.Td([dcc.Textarea(id='msg_input',
                                          value='hello',
                                          style={'height': '50px',
                                                 'width': '400px',
                                                 'background-color': '#F8F8F8',
                                                 'padding': '12px 20px'})]),
                    html.Td([html.Button('Send', id='send_button',
                                         type='submit',
                                         style={'backgroundColor': '#F5F5DC',
                                                'border-radius': '6px',
                                                'font-size': '20px'})],
                            style={'valign': 'middle'})
                ])
            ])],
            style={'max-width': '500px', 'margin': 'auto'}),
        html.Br(),
        html.Div(id='conversation')],
        id='screen',
        style={'width': '400px', 'margin': '0 auto'})
])


def get_model_results(in_text):
    modeling = Modeling(c=c_)
    return modeling.inference(in_text)


@app.callback(Output(component_id='conversation',
                     component_property='children'),
              [Input(component_id='send_button',
                     component_property='n_clicks')],
              State(component_id='msg_input',
                    component_property='value'))
def update_conversation(click, usr_text):
    global conv_hist
    if click:
        bot_response = get_model_results(usr_text)
        usr_text_ = [html.H5(usr_text, style={'text-align': 'left'})]
        bot_response_ = [html.H5(html.I(bot_response),
                                 style={'text-align': 'right',
                                        'font-size': '15px'})]
        conv_hist = usr_text_ + bot_response_ + [html.Hr()] + conv_hist
    return conv_hist


@app.callback(Output(component_id='msg_input',
                     component_property='value'),
              [Input(component_id='conversation',
                     component_property='children')])
def clear_input(_):
    return ''
