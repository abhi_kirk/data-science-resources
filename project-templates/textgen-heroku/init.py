import os
import yaml


class Init:
    def __init__(self, base_dir, data_dir, models_dir, config):
        self.base_dir = base_dir
        self.data_dir = data_dir
        self.models_dir = models_dir
        self.config_name = config
        if not self.check_dirs():
            raise FileNotFoundError("Check Dirs.")
        with open(os.path.join(self.base_dir,
                               self.config_name), 'r') as stream:
            self.config = yaml.safe_load(stream)
        self.mode = self.config['mode']

    def check_dirs(self):
        data_path = os.path.join(self.base_dir, self.data_dir)
        if not os.path.isdir(os.path.join(data_path)):
            return False
        config_path = os.path.join(self.base_dir, self.config_name)
        if not os.path.isfile(config_path):
            return False
        models_path = os.path.join(self.base_dir, self.models_dir)
        if not os.path.isdir(os.path.join(models_path)):
            os.mkdir(models_path)
        return True

    def c(self):
        return self
