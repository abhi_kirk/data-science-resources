# Qualia

## Summary
- Text generation app with semantic language modeling using Tensorflow and LSTMs 
- Dashboard created using Dash, and deployed with Heroku: [CLICK ME!!](https://qualia-aj.herokuapp.com/)
- Model optimization is on-going for a more reasonable text generator!
- Sprint 1 complete

![Text Gen](qualia.png)

## Technical Specs
- python v3.8.12
- heroku v7.59.1

## Deployment Recommendations
- Overview: [Real Python](https://realpython.com/python-dash/)
- Source code should be in root folder in Git (i.e. not in a child directory)
- Addtional files to create:
    - *ProcFile*: [web: gunicorn main:server]
        - *main* is the entry point of the app (main.py), 
        - *server* is a variable inside the file main.py
    ```
    app = dash.Dash()
    server = app.server
    ```
    - *runtime.txt*: [python-3.8.12]
        - Notifies Heroku which python env to create
- ML model files will need to be uploaded to Heroku repo (cannot use *dvc* for these)
- Unable to use dash/HTML code with a class library in the *Viz* class, hence *Viz* is a script and not a class (for now)
- Dash/Heroku instructions for *Hogwarts* coming soon!