# Dash Architecture for a Basic Dockerized Python Web App

Original Article: [Clean Architecture for AI/ML Applications using Dash and Plotly with Docker: Czako Zoltan](https://towardsdatascience.com/clean-architecture-for-ai-ml-applications-using-dash-and-plotly-with-docker-42a3eeba6233)

## Steps
1. Pull down repository 
2. Create new venv using requirements file
3. Install docker (preferably on UNIX host)
4. Go through original article and study code architecture, focusing on:
    1. Model-View-Controller (MVC) design ![MVC](imgs/mvc.png) 
    2. Separation of backend-frontend for web apps
    3. Usage of `docker-compose`, `Dockerfile` and the hidden .env file 
    4. Routing between webpages 
    5. Cache memoizing 
5. Build docker image with: `docker-compose build`
6. Execute container with: `docker-compose up -d`. Refer to the [Docker Tutorial](https://docker-curriculum.com/) if help needed with docker. 
7. Check the website is up and running on local host ip 0.0.0.0, port 8084. 
